// import BBox from './box/BBox.js';

export default class Box {

  constructor(a, b) {
    this._a = a;
    this._b = b;
  }

  getSquare() {
    return this._a * this._b;
  }

  // static getInstance(a, b) {
  //   return new BBox(a, b);
  // }
}
