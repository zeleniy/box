import Box from '../Box.js';

export default class BBox extends Box {

  getSquare() {
    return this._a * this._b + 1;
  }
}
