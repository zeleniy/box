var path = require('path');

module.exports = {
  entry: './index.js',
  output: {
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  module : {
    loaders: [{
      test   : /\.js$/,
      exclude: /node_modules/,
      loader : 'babel-loader',
      query: {
        presets: ['es2015', 'stage-0']
      }
    }]
  }
};
